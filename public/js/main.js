var config = {
    apiKey: "AIzaSyBFcRF0q8iBVIzZiWB4bI4XOYY_KdiNvX0",
    authDomain: "guillermogarrido-100e8.firebaseapp.com",
    databaseURL: "https://guillermogarrido-100e8.firebaseio.com",
    projectId: "guillermogarrido-100e8",
    storageBucket: "guillermogarrido-100e8.appspot.com",
    messagingSenderId: "983191689594"
};
firebase.initializeApp(config);

function registrarUsuario(email, password, idMensaje) {
    firebase.auth().createUserWithEmailAndPassword(email, password).then(
            function (usuario) {
                console.log(usuario);
                var usr = {};
                usr.correo = usuario.email;
                firebase.database().ref().child("usuarios").push().set(usr);
            },
            function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                $("#" + idMensaje).html(error.message + error.code);
                // ...
            });
}

function iniciarSesiónFacebook() {
    var provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('user_birthday');
    firebase.auth().signInWithPopup(provider).then(function (result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        var usr = {};
        usr.correo = user.email;
        usr.nombre = user.displayName;
        usr.foto = user.photoURL;
        usr.token = token;
        firebase.database().ref().child("usuarios").child(user.uid).set(usr,
                function (error) {
                    console.log(error);
                    window.location = "salas.html";
                });
        // ...
    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        $("#mensajes").html(error.message + error.code);
        // ...
    });
}

function cerrarSesion() {
    firebase.auth().signOut().then(function () {
        console.log("Cerro sesión");
    }).catch(function (error) {
        console.log("ERROR al cerrar sesión");
    });
}


function consultarSalas() {
    firebase.database().ref().child("salas").on("value", function (salasSnapShot) {
        $("#salas").html("");
        salasSnapShot.forEach(function (sala) {
            console.log(sala.val());
            $("#salas").append(
                    '<li class="media" onclick="abrirChat(\'' + sala.key +'\');">' +
                    '<div class="media-left">' +
                    '<img style class="media-object imgCircular" src="' +
                    sala.val().urlFoto
                    + '" >' +
                    '</div>' +
                    '<div class="media-body">' +
                    '<h4 class="media-heading">' + sala.val().nombreSala + '</h4>' +
                    'Personas: ' + sala.val().numPersonas +
                    '</div>' +
                    '</li>');
        });
    });
}
function abrirChat(sala){
    window.location="chat.html?sala=" + sala;
}
function registrarSala(nombre) {
//    var salaObj={};
//    salaObj.nombreSala=nombre;
//var salaObj = {nombreSala:nombre, numPersonas:0}
    firebase.database().ref().child("salas").push().set(
            {
                nombreSala: nombre,
                numPersonas: 0,
                urlFoto: "https://www.voicechatapi.com/static/img/chat.png"
            });
}
function mandarMensaje(mensaje, sala) {
    firebase.database().ref().child("mensajes").child(sala).push().set(
            {
                msg: mensaje,
                urlUsuario: firebase.auth().currentUser.photoURL,
                timeStr: new Date().toUTCString(),
                timeLong: new Date().getTime(),
                nombreUsuario: firebase.auth().currentUser.displayName
            });
}

function uploadFile(input, path, callback) {
    var files = Array.from(input.get(0).files);
    
    storage = firebase.storage();
    
    storageRef = storage.ref();
    files.forEach(function (file) {
        var uploadTask = storageRef.child(path).child(file.name).put(file);
        uploadTask.on('state_changed', function (snapshot) {
            var progress = ((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
            progress = parseInt(progress);
            $("#progressPhotos").css("width", progress + "%");

        }, function (error) {
            console.log(error);
        }, function () {
            $("#progressPhotos").css("width", "100%");
            var downloadURL = uploadTask.snapshot.downloadURL;
            callback(downloadURL, uploadTask.snapshot.metadata.name);
        });
    });
}
function getUrlVars() {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
return vars;
}